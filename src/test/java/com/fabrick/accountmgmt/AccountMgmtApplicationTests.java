package com.fabrick.accountmgmt;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fabrick.accountmgmt.handler.AccountHandler;
import com.fabrick.accountmgmt.model.Account;
import com.fabrick.accountmgmt.model.AccountsResponse;
import com.fabrick.accountmgmt.model.Balance;
import com.fabrick.accountmgmt.model.BalanceResponse;
import com.fabrick.accountmgmt.model.LegalPersonBeneficiary;
import com.fabrick.accountmgmt.model.MoneyTransferRequest;
import com.fabrick.accountmgmt.model.MoneyTransferResponse;
import com.fabrick.accountmgmt.model.NaturalPersonBeneficiary;
import com.fabrick.accountmgmt.model.Payload;
import com.fabrick.accountmgmt.model.Status;
import com.fabrick.accountmgmt.model.TaxRelief;
import com.fabrick.accountmgmt.model.Transaction;
import com.fabrick.accountmgmt.model.TransactionsRequest;
import com.fabrick.accountmgmt.model.TransactionsResponse;

@SpringBootTest
class AccountMgmtApplicationTests {
	
	@Autowired
	AccountHandler accountHandler;
	
	@Test
	void retrieveAccounts() {
		AccountsResponse response = accountHandler.getAccounts();
		Status status = response.getStatus();
		ArrayList<Account> accounts = response.getPayload();
		Account account = accounts.get(0);
		
		assertThat(status.getCode()).isEqualTo("OK");
		assertThat(status.getDescription()).isEqualTo("Accounts list retrieved successfully");

		assertThat(account.getAccountId()).isEqualTo(14537780);
		assertThat(account.getIban()).isEqualTo("IT40L0326822311052923800661");
		assertThat(account.getAccount()).isEqualTo("1152923800661");
		assertThat(account.getAccountMasked()).isEqualTo("Not Available");
		assertThat(account.getAccountAlias()).isEqualTo("Test api");
		assertThat(account.getProductName()).isEqualTo("Conto Websella");
		assertThat(account.getAccountHolderName()).isEqualTo("LUCA TERRIBILE");
		assertThat(account.getCurrency()).isEqualTo("Euro");
	}
	
	@Test
	void retrieveBalance() {
		BalanceResponse response = accountHandler.getBalance("14537780");
		Status status = response.getStatus();
		ArrayList<Balance> balances = response.getPayload();
		Balance balance = balances.get(0);

		assertThat(status.getCode()).isEqualTo("OK");
		assertThat(status.getDescription()).isEqualTo("Account balance retrieved successfully");

		assertThat(balance.getDate()).isEqualTo(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		assertThat(balance.getBalance()).isEqualTo(8.6);
		assertThat(balance.getAvailableBalance()).isEqualTo(8.6);		
	}
	
	@Test
	void retrieveTransactions() {
		TransactionsRequest transactionsRequest = new TransactionsRequest();
		transactionsRequest.setAccountId("14537780");
		transactionsRequest.setFromDate("01/01/2019");
		transactionsRequest.setToDate("01/12/2019");
		
		TransactionsResponse response = accountHandler.getTransactions(transactionsRequest);
		Status status = response.getStatus();
		ArrayList<Payload> payloadList = response.getPayload();
		Payload payload = payloadList.get(0);
		ArrayList<Transaction> transactions = payload.getTransactions();
		Transaction transaction = transactions.get(0);
		
		assertThat(transactions).hasSize(14);

		assertThat(status.getCode()).isEqualTo("OK");
		assertThat(status.getDescription()).isEqualTo("Account Transactions retrieved successfully");

		assertThat(transaction.getTransactionId()).isEqualTo("00000000282831");
		assertThat(transaction.getAccountingDate()).isEqualTo("29/11/2019");
		assertThat(transaction.getValueDate()).isEqualTo("01/12/2019");
		assertThat(transaction.getAmount()).isEqualTo("-343,77");
		assertThat(transaction.getCurrency()).isEqualTo("EUR");
		assertThat(transaction.getDescription()).isEqualTo("PD VISA CORPORATE 10");		
	}
	
	@Test
	void createMoneyTransfer() {
		
		NaturalPersonBeneficiary naturalPersonBeneficiary = new NaturalPersonBeneficiary();
		naturalPersonBeneficiary.setFiscalCode1("ABCDEF81L04A859O");
		naturalPersonBeneficiary.setFiscalCode2("");
		naturalPersonBeneficiary.setFiscalCode3("");
		naturalPersonBeneficiary.setFiscalCode4("");
		naturalPersonBeneficiary.setFiscalCode5("");
		
		LegalPersonBeneficiary legalPersonBeneficiary = new LegalPersonBeneficiary();
		legalPersonBeneficiary.setFiscalCode("");
		legalPersonBeneficiary.setLegalRepresentativeFiscalCode("");
		
		TaxRelief taxRelief = new TaxRelief();
		taxRelief.setTaxReliefId("L027");
		taxRelief.setReceiverFiscalCode("45632198758");
		taxRelief.setBeneficiaryType("NATURAL_PERSON");
		taxRelief.setNaturalPersonBeneficiary(naturalPersonBeneficiary);
		taxRelief.setLegalPersonBeneficiary(null);
		
		MoneyTransferRequest request = new MoneyTransferRequest();
		request.setAccountId("14537780");
		request.setReceiverIban("IT23A0336844430152923804660");
		request.setReceiverBic("");
		request.setReceiverSwift("");
		request.setReceiverName("John Doe");
		request.setDescription("Payment invoice 75/2017");
		request.setAmount("800.00");
		request.setCurrency("EUR");
		request.setExecutionDate("26/10/2017");
		request.setUrgent("false");
		request.setInstant("false");
		request.setFeeType("");
		request.setReceiverAddress("");
		request.setReceiverCity("");
		request.setReceiverCountry("");
		request.setTaxRelief(taxRelief);
		
		MoneyTransferResponse response = accountHandler.createMoneyTransfer(request);
		Status status = response.getStatus();
		com.fabrick.accountmgmt.model.Error error = response.getErrors().get(0);

		assertThat(status.getCode()).isEqualTo("KO");
		assertThat(status.getDescription()).isEqualTo("SCT Transfer request failed");

		assertThat(error.getCode()).isEqualTo("API000");
		assertThat(error.getDescription()).isEqualTo("Coordinate IBAN errate o non aggiornate, non è possibile ricavare un BIC valido. Contattare il beneficiario per ottenere le coordinate IBAN aggiornate.");		
		
	}
}
