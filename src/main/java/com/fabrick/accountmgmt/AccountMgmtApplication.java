package com.fabrick.accountmgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountMgmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountMgmtApplication.class, args);
	}

}
