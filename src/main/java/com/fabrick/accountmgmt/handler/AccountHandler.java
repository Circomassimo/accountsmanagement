package com.fabrick.accountmgmt.handler;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.fabrick.accountmgmt.model.AccountsResponse;
import com.fabrick.accountmgmt.model.BalanceResponse;
import com.fabrick.accountmgmt.model.MoneyTransferRequest;
import com.fabrick.accountmgmt.model.MoneyTransferResponse;
import com.fabrick.accountmgmt.model.TransactionsRequest;
import com.fabrick.accountmgmt.model.TransactionsResponse;

@Component
public class AccountHandler extends GenericHandler {
	
	private static String BASE_URL = "https://sandbox.platfr.io";
	
	public AccountsResponse getAccounts() {
		String url = BASE_URL + "/api/gbs/banking/v2/accounts";
		return execCall(url, HttpMethod.GET, null, AccountsResponse.class );
	}
	
	public BalanceResponse getBalance(String accountId) {
		String url = BASE_URL + "/api/gbs/banking/v2/accounts/" + accountId + "/balance";
		return execCall(url, HttpMethod.GET, null, BalanceResponse.class);
	}

	public TransactionsResponse getTransactions(TransactionsRequest transactionsRequest) {
		String url = BASE_URL + "/api/gbs/banking/v2/accounts/" + transactionsRequest.getAccountId() + "/transactions";
		String uriQuery = "?fromDate=" + transactionsRequest.getFromDate() + "&toDate=" + transactionsRequest.getToDate();
		return execCall(url+uriQuery, HttpMethod.GET, null, TransactionsResponse.class);
	}

	public MoneyTransferResponse createMoneyTransfer(MoneyTransferRequest moneyTransferRequest) {
		String url = BASE_URL + "/api/gbs/banking/v2.1/accounts/" + moneyTransferRequest.getAccountId() + "/payments/sct/orders";
		return execCall(url, HttpMethod.POST, moneyTransferRequest, MoneyTransferResponse.class);
	}

}
