package com.fabrick.accountmgmt.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fabrick.accountmgmt.error.RestTemplateResponseErrorHandler;

public class GenericHandler {
	
	private static String AUTH_SCHEMA = "S2S";
	private static String API_KEY = "FXOVVXXHVCPVPBZXIJOBGUGSKHDNFRRQJP";
	
	@Autowired
	private RestTemplateResponseErrorHandler responseErrorHandler;
	
	protected <R,T> T execCall(String endpoint, HttpMethod method, R reqParam, Class<T> clazz) {
		HttpHeaders	headers = new HttpHeaders();
		headers.set("Auth-Schema", AUTH_SCHEMA);
		headers.set("apikey", API_KEY);
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<R> entity = null;

		if( null == reqParam ) {
			entity =  new HttpEntity<R>( headers );
		} else {
			entity =  new HttpEntity<R>( reqParam, headers );
		}

		RestTemplate restTemplate = new RestTemplateBuilder()
				.errorHandler(responseErrorHandler)
				.build();
		ResponseEntity<T> response = restTemplate.exchange( endpoint, method, entity, clazz );

		return response.getBody();
	}
}
