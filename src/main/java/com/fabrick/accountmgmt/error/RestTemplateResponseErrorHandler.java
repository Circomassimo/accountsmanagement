package com.fabrick.accountmgmt.error;

import java.io.IOException;

import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler{

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return ( response.getStatusCode().series() == Series.CLIENT_ERROR || 
				response.getStatusCode().series() == Series.SERVER_ERROR);
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		if (response.getStatusCode().series() == Series.SERVER_ERROR) {
			//Add some logic if needed
		} else if (response.getStatusCode().series() == Series.CLIENT_ERROR) {
			//Add some logic if needed
        }
	}

}
