package com.fabrick.accountmgmt.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoneyTransferResponse extends BaseResponse {
	
	private List payload;
	
	public MoneyTransferResponse() {}

	public List getPayload() {
		return payload;
	}

	public void setPayload(List payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "MoneyTransferResponse [payload="
				+ (payload != null ? payload.subList(0, Math.min(payload.size(), maxLen)) : null) + ", getStatus()="
				+ getStatus() + ", getErrors()="
				+ (getErrors() != null ? getErrors().subList(0, Math.min(getErrors().size(), maxLen)) : null) + "]";
	}
	
}
