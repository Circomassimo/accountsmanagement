package com.fabrick.accountmgmt.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BalanceResponse extends BaseResponse {
	
	private ArrayList<Balance> payload;
	
	public BalanceResponse() {}

	public ArrayList<Balance> getPayload() {
		return payload;
	}

	public void setPayload(ArrayList<Balance> payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "BalanceResponse [payload="
				+ (payload != null ? payload.subList(0, Math.min(payload.size(), maxLen)) : null) + "]";
	}
		
}
