package com.fabrick.accountmgmt.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountsResponse extends BaseResponse {
	
	private ArrayList<Account> payload;
	
	public AccountsResponse() {}

	public ArrayList<Account> getPayload() {
		return payload;
	}

	public void setPayload(ArrayList<Account> payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "AccountsResponse [payload="
				+ (payload != null ? payload.subList(0, Math.min(payload.size(), maxLen)) : null) + ", getStatus()="
				+ getStatus() + ", getErrors()="
				+ (getErrors() != null ? getErrors().subList(0, Math.min(getErrors().size(), maxLen)) : null)
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}	
	
}
