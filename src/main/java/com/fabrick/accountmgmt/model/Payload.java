package com.fabrick.accountmgmt.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Payload {
	
	private ArrayList<Transaction> transactions;
	
	public Payload(){
		
	}

	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<Transaction> transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "TransactionList [transactions="
				+ (transactions != null ? transactions.subList(0, Math.min(transactions.size(), maxLen)) : null) + "]";
	}

}
