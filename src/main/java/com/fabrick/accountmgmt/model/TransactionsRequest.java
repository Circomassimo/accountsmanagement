package com.fabrick.accountmgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionsRequest {
	
	private String accountId;
	private String fromDate;
	private String toDate;
	
	public TransactionsRequest() {
		
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	@Override
	public String toString() {
		return "TransactionsRequest [accountId=" + accountId + ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}
}
