package com.fabrick.accountmgmt.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionsResponse extends BaseResponse {
	
	private ArrayList<Payload> payload;
	
	public TransactionsResponse() {}

	public ArrayList<Payload> getPayload() {
		return payload;
	}

	public void setPayload(ArrayList<Payload> payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "TransactionsResponse [payload="
				+ (payload != null ? payload.subList(0, Math.min(payload.size(), maxLen)) : null) + "]";
	}
}
