package com.fabrick.accountmgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {

	private Long accountId;
	private String iban;
	private String account;
	private String accountMasked;
	private String accountAlias;
	private String productName;
	private String accountHolderName;
	private String currency;
	
	public Account(){
		
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAccountMasked() {
		return accountMasked;
	}

	public void setAccountMasked(String accountMasked) {
		this.accountMasked = accountMasked;
	}

	public String getAccountAlias() {
		return accountAlias;
	}

	public void setAccountAlias(String accountAlias) {
		this.accountAlias = accountAlias;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", iban=" + iban + ", account=" + account + ", accountMasked="
				+ accountMasked + ", accountAlias=" + accountAlias + ", productName=" + productName
				+ ", accountHolderName=" + accountHolderName + ", currency=" + currency + "]";
	}

}
