package com.fabrick.accountmgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Balance {
	
	private String date;
	private double balance;
	private double availableBalance;
	
	public Balance() {}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(double availableBalance) {
		this.availableBalance = availableBalance;
	}

	@Override
	public String toString() {
		return "Balance [date=" + date + ", balance=" + balance + ", availableBalance=" + availableBalance + "]";
	}

}
