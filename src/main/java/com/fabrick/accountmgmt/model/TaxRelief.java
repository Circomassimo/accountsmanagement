package com.fabrick.accountmgmt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxRelief {
	
	private String taxReliefId;
	private String receiverFiscalCode;
	private String beneficiaryType;
	private NaturalPersonBeneficiary naturalPersonBeneficiary;
	private LegalPersonBeneficiary legalPersonBeneficiary;
	
	public TaxRelief() {
	}

	public String getTaxReliefId() {
		return taxReliefId;
	}

	public void setTaxReliefId(String taxReliefId) {
		this.taxReliefId = taxReliefId;
	}

	public String getReceiverFiscalCode() {
		return receiverFiscalCode;
	}

	public void setReceiverFiscalCode(String receiverFiscalCode) {
		this.receiverFiscalCode = receiverFiscalCode;
	}

	public String getBeneficiaryType() {
		return beneficiaryType;
	}

	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}

	public NaturalPersonBeneficiary getNaturalPersonBeneficiary() {
		return naturalPersonBeneficiary;
	}

	public void setNaturalPersonBeneficiary(NaturalPersonBeneficiary naturalPersonBeneficiary) {
		this.naturalPersonBeneficiary = naturalPersonBeneficiary;
	}

	public LegalPersonBeneficiary getLegalPersonBeneficiary() {
		return legalPersonBeneficiary;
	}

	public void setLegalPersonBeneficiary(LegalPersonBeneficiary legalPersonBeneficiary) {
		this.legalPersonBeneficiary = legalPersonBeneficiary;
	}

	@Override
	public String toString() {
		return "TaxRelief [taxReliefId=" + taxReliefId + ", receiverFiscalCode=" + receiverFiscalCode
				+ ", beneficiaryType=" + beneficiaryType + ", naturalPersonBeneficiary=" + naturalPersonBeneficiary
				+ ", legalPersonBeneficiary=" + legalPersonBeneficiary + "]";
	}
	
		
}
