package com.fabrick.accountmgmt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fabrick.accountmgmt.handler.AccountHandler;
import com.fabrick.accountmgmt.model.AccountsResponse;
import com.fabrick.accountmgmt.model.BalanceResponse;
import com.fabrick.accountmgmt.model.MoneyTransferRequest;
import com.fabrick.accountmgmt.model.MoneyTransferResponse;
import com.fabrick.accountmgmt.model.TransactionsRequest;
import com.fabrick.accountmgmt.model.TransactionsResponse;

@RestController
@RequestMapping("/account")
public class AccountRestController {
	
	@Autowired
	AccountHandler accountHandler;

	//private static final Logger log = LoggerFactory.getLogger(AccountRestController.class);
	
	@RequestMapping(produces = "application/json", value = { "/accounts" }, method = RequestMethod.GET )
	public @ResponseBody AccountsResponse getAccounts() {
        return accountHandler.getAccounts();
	}
	
	@RequestMapping(produces = "application/json", value = { "/balance/{accountId}" }, method = RequestMethod.GET )
	public @ResponseBody BalanceResponse getBalance(@PathVariable String accountId) {
        return accountHandler.getBalance(accountId);
	}
	
	@RequestMapping(produces = "application/json", consumes = "application/json", value = { "/transactions" }, method = RequestMethod.GET )
	public @ResponseBody TransactionsResponse getTransactions(@RequestBody TransactionsRequest transactionsRequest) {
        return accountHandler.getTransactions(transactionsRequest);
	}
	
	@RequestMapping(produces = "application/json", consumes = "application/json", value = { "/moneyTransfer/create" }, method = RequestMethod.POST )
	public @ResponseBody MoneyTransferResponse createMoneyTransfer(@RequestBody MoneyTransferRequest moneyTransferRequest) {
        return accountHandler.createMoneyTransfer(moneyTransferRequest);
	}
}
